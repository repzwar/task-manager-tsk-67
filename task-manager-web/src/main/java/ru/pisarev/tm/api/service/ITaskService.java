package ru.pisarev.tm.api.service;

import ru.pisarev.tm.model.Task;

public interface ITaskService extends IRecordService<Task> {

}
