package ru.pisarev.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.pisarev.tm.model.Project;

import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface IProjectEndpoint {

    @GetMapping("/findAll")
    List<Project> findAll();

    @GetMapping("/find/{id}")
    Project find(@PathVariable("id") String id);

    @PostMapping("/create")
    Project create(@RequestBody Project project);

    @PostMapping("/createAll")
    List<Project> createAll(@RequestBody List<Project> projects);

    @PostMapping("/save")
    Project save(@RequestBody Project project);

    @PostMapping("/saveAll")
    List<Project> saveAll(@RequestBody List<Project> projects);

    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") String id);

    @PostMapping("/deleteAll")
    void deleteAll();

}
