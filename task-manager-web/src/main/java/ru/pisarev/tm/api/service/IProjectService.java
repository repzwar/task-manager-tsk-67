package ru.pisarev.tm.api.service;

import ru.pisarev.tm.model.Project;

public interface IProjectService extends IRecordService<Project> {

}
