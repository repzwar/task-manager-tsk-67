package ru.pisarev.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.service.ProjectService;
import ru.pisarev.tm.service.TaskService;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private TaskService service;

    @Autowired
    private ProjectService projectService;

    @GetMapping("/task/create")
    public String create() {
        service.add(new Task("New Task" + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        service.removeById(id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id
    ) {
        final Task task = service.findById(id);
        return new ModelAndView("task-edit", "task", task);
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") Task task, BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        service.add(task);
        return "redirect:/tasks";
    }

    @ModelAttribute("projects")
    public Collection<Project> getProjects() {
        return projectService.findAll();
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

}
