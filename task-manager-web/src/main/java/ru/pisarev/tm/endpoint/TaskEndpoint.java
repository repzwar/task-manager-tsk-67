package ru.pisarev.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.pisarev.tm.api.endpoint.ITaskEndpoint;
import ru.pisarev.tm.model.Task;
import ru.pisarev.tm.service.TaskService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskEndpoint implements ITaskEndpoint {

    @Autowired
    private TaskService service;

    @Override
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return new ArrayList<>(service.findAll());
    }

    @Override
    @GetMapping("/find/{id}")
    public Task find(@PathVariable("id") final String id) {
        return service.findById(id);
    }

    @Override
    @PostMapping("/create")
    public Task create(@RequestBody final Task task) {
        service.add(task);
        return task;
    }

    @Override
    @PostMapping("/createAll")
    public List<Task> createAll(@RequestBody final List<Task> tasks) {
        service.addAll(tasks);
        return tasks;
    }

    @Override
    @PutMapping("/save")
    public Task save(@RequestBody final Task task) {
        service.add(task);
        return task;
    }

    @Override
    @PutMapping("/saveAll")
    public List<Task> saveAll(@RequestBody final List<Task> tasks) {
        service.addAll(tasks);
        return tasks;
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") final String id) {
        service.removeById(id);
    }

    @Override
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        service.clear();
    }

}
