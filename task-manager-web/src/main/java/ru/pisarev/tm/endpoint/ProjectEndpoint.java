package ru.pisarev.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.pisarev.tm.api.endpoint.IProjectEndpoint;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.service.ProjectService;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "ru.pisarev.tm.api.endpoint." +
        "IProjectEndpoint")
@RestController
@RequestMapping("/api/projects")
public class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private ProjectService service;

    @Override
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return new ArrayList<>(service.findAll());
    }

    @Override
    @GetMapping("/find/{id}")
    public Project find(@PathVariable("id") final String id) {
        return service.findById(id);
    }

    @Override
    @PostMapping("/create")
    public Project create(@RequestBody final Project project) {
        service.add(project);
        return project;
    }

    @Override
    @PostMapping("/createAll")
    public List<Project> createAll(@RequestBody final List<Project> projects) {
        service.addAll(projects);
        return projects;
    }

    @Override
    @PutMapping("/save")
    public Project save(@RequestBody final Project project) {
        service.add(project);
        return project;
    }

    @Override
    @PutMapping("/saveAll")
    public List<Project> saveAll(@RequestBody final List<Project> projects) {
        service.addAll(projects);
        return projects;
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") final String id) {
        service.removeById(id);
    }

    @Override
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        service.clear();
    }

}
