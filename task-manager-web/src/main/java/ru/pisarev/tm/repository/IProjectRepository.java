package ru.pisarev.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pisarev.tm.model.Project;

public interface IProjectRepository extends JpaRepository<Project, String> {

}
