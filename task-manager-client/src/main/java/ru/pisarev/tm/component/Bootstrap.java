package ru.pisarev.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.constant.TerminalConst;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.exception.system.UnknownListenerException;
import ru.pisarev.tm.listener.AbstractListener;
import ru.pisarev.tm.service.ListenerService;
import ru.pisarev.tm.service.LogService;
import ru.pisarev.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;

import static ru.pisarev.tm.util.SystemUtil.getPID;
import static ru.pisarev.tm.util.TerminalUtil.displayWait;
import static ru.pisarev.tm.util.TerminalUtil.displayWelcome;

@Component
public final class Bootstrap {

    @Nullable
    @Autowired
    protected FileScanner fileScanner;

    @Nullable
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private ListenerService listenerService;

    @NotNull
    @Autowired
    private LogService logService;

    public void start(String... args) {
        displayWelcome();
        initListeners();
        if (runArgs(args)) System.exit(0);
        process();
    }

    public void initApplication() {
        initPID();
    }

    @SneakyThrows
    private void initListeners() {
        if (listeners == null) return;
        Arrays.stream(listeners).filter(Objects::nonNull).forEach(t -> listenerService.add(t));
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private boolean runArgs(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final AbstractListener listener = listenerService.getListenerByArg(args[0]);
        if (listener == null) throw new UnknownListenerException(args[0]);
        publisher.publishEvent(new ConsoleEvent(listener.name()));
        return true;
    }

    public void runCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractListener listener = listenerService.getListenerByName(command);
        if (listener == null) throw new UnknownListenerException(command);
        publisher.publishEvent(new ConsoleEvent(command));
    }

    private void process() {
        logService.debug("Test environment.");
        @Nullable String command = "";
        fileScanner.init();
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            try {
                displayWait();
                command = TerminalUtil.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
    }

}
