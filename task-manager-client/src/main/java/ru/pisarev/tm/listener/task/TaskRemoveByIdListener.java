package ru.pisarev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.endpoint.TaskEndpoint;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.listener.TaskAbstractListener;
import ru.pisarev.tm.util.TerminalUtil;

@Component
public class TaskRemoveByIdListener extends TaskAbstractListener {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by id.";
    }

    @Override
    @EventListener(condition = "@taskRemoveByIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("Enter id");
        @Nullable final String id = TerminalUtil.nextLine();
        taskEndpoint.removeTaskById(getSession(), id);
    }

}
